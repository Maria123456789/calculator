const calculator = document.querySelector('.calculator')
const keys = calculator.querySelector('.calculator__keys')
const display = document.querySelector('.calculator__display')

display.addEventListener('keydown', e => {
	e.preventDefault()
	if (e.key.match(/^\d$/)) {
		handleInput(null, e.key)
	} else if (e.key.match(/^[+-/*]$/)) {
		handleInput(({'+': 'add', '-': 'substract', '/': 'divide', '*': 'multiply'})[e.key], e.key)
	} else if (e.key.match(/^\.$/)) {
		handleInput(null, 'decimal')
	} else if(e.key === 'Enter') {
		handleInput('calculate', '=')		
	}
})

keys.addEventListener('click', e => {
	if (e.target.matches('button')) {
		const key = e.target
		const action = key.dataset.action
		const keyContent = key.textContent
    	handleInput(action, keyContent, key)
 	}
 	display.focus();
})

const handleInput = (action, keyContent, key) => {
	const displayedNum = display.value
	const previousKeyType = calculator.dataset.previousKeyType

	if (!action) {
		if (displayedNum === '0' || previousKeyType === 'operator' || previousKeyType === 'calculate') {
			display.value = setDisplayValue(keyContent)
		} else {
			display.value = setDisplayValue(displayedNum + keyContent)
		}
		calculator.dataset.previousKeyType = 'number'
	}

	if (
		action === 'add' ||
		action === 'subtract' ||
		action === 'multiply' ||
		action === 'divide'
	) {
		const firstValue = calculator.dataset.firstValue
		const operator = calculator.dataset.operator
		const secondValue = displayedNum

		if (firstValue && operator && previousKeyType !== 'operator' && previousKeyType !== 'calculate') {
			const calcValue = calculate(firstValue, operator, secondValue)
			display.value = setDisplayValue(calcValue)
			calculator.dataset.firstValue = calcValue
		} else {
			calculator.dataset.firstValue = displayedNum
		}

		if(key) {
			key.classList.add('is-depressed')
		}

		calculator.dataset.previousKeyType = 'operator'
		calculator.dataset.operator = action
	}

	if(key) {
		Array.from(key.parentNode.children)
	  		.forEach(k => k.classList.remove('is-depressed'))
	}

	if (action === 'decimal') {
		if (!displayedNum.includes('.')) {
			display.value = setDisplayValue(displayedNum + '.'  				)
		} else if (previousKeyType === 'operator' || previousKeyType === 'calculate') {
			display.value = setDisplayValue('0.')
		}

		calculator.dataset.previousKeyType = 'decimal'
	}

	if (action !== 'clear') {
		const clearButton = calculator.querySelector('[data-action=clear]')
		clearButton.textContent = 'CE'
	}

	if (action === 'clear') {
		if (key.textContent === 'AC') {
			calculator.dataset.firstValue = ''
			calculator.dataset.modValue = ''
			calculator.dataset.operator = ''
			calculator.dataset.previousKeyType = ''
		} else {
			key.textContent = 'AC'
		}

		display.value = setDisplayValue(0)

		calculator.dataset.previousKeyType = 'clear'
	}

	if (action === 'calculate') {
		let firstValue = calculator.dataset.firstValue
		const operator = calculator.dataset.operator
		let secondValue = displayedNum

		if (firstValue) {
			if (previousKeyType === 'calculate') {
  				firstValue = displayedNum
  				secondValue = calculator.dataset.modValue
			}

			display.value = setDisplayValue(calculate(firstValue, operator, secondValue))
		}

		calculator.dataset.modValue = secondValue
		calculator.dataset.previousKeyType = 'calculate'
	}
}

const setDisplayValue = value => {
	let str = String(value)
	if (String(value).length > 7) {
		alert('number too long')
		return str.substring(0, 7)
	}
	return str;
}

const calculate = (n1, operator, n2) => {

	let result = ''

	if (operator === 'add') {
		result = parseFloat(n1) + parseFloat(n2)
	} else if (operator === 'subtract') {
		result = parseFloat(n1) - parseFloat(n2)
	} else if (operator === 'multiply') {
		result = parseFloat(n1) * parseFloat(n2)
	} else if (operator === 'divide') {
		let floatn2 = parseFloat(n2)
		if (floatn2 === 0) {
			alert('division by 0')
			return n1
		}
		result = parseFloat(n1) / floatn2
	}

	return result
}


